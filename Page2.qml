import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2 // подсказка конт пробел
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.12
import QtWebView 1.1

Page {
    id:page2
    header: ToolBar{
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 50
            implicitWidth: 100
            color: "#00a88e"
        }
        GridLayout{
            columns: 3
            anchors.fill:parent
            ToolButton {
                Layout.alignment: Qt.AlignLeft
                Layout.columnSpan: 3
                id: points4
                text: qsTr("⇦")
                x: 10
                y: 1
                font.pixelSize: 40
            }
            
            Label{
                Layout.columnSpan: 3
                Layout.row: 0
                text: "Видосики"
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }
            
            Row
            {
                Layout.column: 2
                Layout.alignment: Qt.AlignRight
                ToolButton {
                    id: points5
                    text: qsTr("✮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                }
                
                ToolButton {
                    id: points6
                    text: qsTr("⋮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                    
                    
                    
                }
            }
            //
        }
    }
    
    GridLayout{
        anchors.fill:parent
        columns: 3
        rows: 4
        RowLayout{
            Layout.columnSpan:3
            Layout.row:0
            Layout.alignment: Qt.AlignCenter
            
            RadioButton{
                id:radio2
                text: "Видео"
                onClicked: {
                    columnlay1.visible = true
                    mediaplayer.playbackState === mediaplayer.play()
                    columnlay2.visible=false
                    button.visible=true
                    videoSlider.visible =true
                    volumeSlider.visible=true
                    positionLabel.visible=true
                    camera.stop()
                    buttoncamera.visible=false
                    openButton.visible=true
                    
                }
                
            }
            RadioButton{
                id:radio1
                text: "Камера"
                onClicked: {
                    columnlay1.visible = false
                    mediaplayer.playbackState === mediaplayer.pause()
                    columnlay2.visible=true
                    camera.start()
                    button.visible=false
                    videoSlider.visible =false
                    volumeSlider.visible=false
                    positionLabel.visible=false
                    buttoncamera.visible=true
                    openButton.visible=false
                    buttonvideomake.visible = true
                }
                
            }
            
        }
        ColumnLayout{
            id:columnlay1
            
            Layout.columnSpan: 3
            Layout.row: 1
            
            MediaPlayer {
                id: mediaplayer
                source: "video/sample.mp4"
                volume: volumeSlider.volume
            }
            
            VideoOutput {
                Layout.fillWidth: true
                source: mediaplayer
                id:videooutput
                
            }
            
            MouseArea {
                id: playArea
                anchors.fill: parent
                onPressed: mediaplayer.play();
            }
            
        }
        ColumnLayout{
            id:columnlay2
            visible: false
            Layout.columnSpan: 3
            Layout.row: 1
            
            Rectangle {
                Layout.fillWidth: true
                Layout.fillHeight:  true
                
                
                
                Camera {
                    
                    
                    videoRecorder.audioEncodingMode: CameraRecorder.ConstantBitrateEncoding;
                    videoRecorder.audioBitRate: 128000
                    videoRecorder.mediaContainer: "mp4"
                    id:  camera
                    captureMode: stop()
                    imageCapture {
                        onImageCaptured: {
                            // Show the preview in an Image
                            photoPreview1.source = preview
                        }
                    }
                }
                
                VideoOutput {
                    id: videoCam
                    source:  camera
                    focus :  visible // to receive focus and capture key events when visible
                    anchors.fill: parent
                }
                
                Image {
                    Layout.column: 1
                    Layout.row: 0
                    id:  photoPreview1
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: 150
                    width: 200
                }
                MouseArea{
                    
                }
                
            }
            
        }
        RowLayout{
            Layout.alignment: Qt.AlignCenter
            
            Layout.columnSpan: 3
            Layout.row: 3
            Button{
                visible: false
                id:buttoncamera
                onClicked: camera.imageCapture.captureToLocation("C:/Users/victo/Savushkin_V_V_181_331_MObilusKoks_Dev/camera")
                text: "сфоткать"
                
            }
            //                     Button{
            
            //                         visible: false
            //                         id:buttonvideomake
            //                         text: camera.cameraState === Camera.cameraState ? "Снять" : "Стоп"
            //                         onClicked: camera.cameraState === Camera.cameraState ? camera.videoRecorder.record() : camera.videoRecorder.stop()
            //                     }
            //                     Button{
            //                         onClicked: camera.videoRecorder.outputLocation
            //                     }
            
            
        }
        
        Slider{
            Layout.columnSpan: 3
            Layout.row: 2
            Layout.fillWidth: true
            id:videoSlider
            to: mediaplayer.duration
            property bool sync: false
            onValueChanged: {
                if (!sync)
                    mediaplayer.seek(value)
            }
            Connections{
                target: mediaplayer
                onPositionChanged: {
                    videoSlider.sync = true
                    videoSlider.value = mediaplayer.position
                    videoSlider.sync = false
                }
            }
            
        }
        Button{
            Layout.margins: 10
            Layout.column: 0
            Layout.row: 3
            id:button
            enabled: mediaplayer.hasVideo
            Layout.preferredWidth: button.implicitHeight
            text: mediaplayer.playbackState === MediaPlayer.PlayingState ? "☭" : "►"
            onClicked: mediaplayer.playbackState === MediaPlayer.PlayingState ? mediaplayer.pause() : mediaplayer.play()
            
        }
        Slider{
            Layout.column: 1
            Layout.row: 3
            id: volumeSlider
            property real volume: QtMultimedia.convertVolume(volumeSlider.value,
                                                             QtMultimedia.LogarithmicVolumeScale,
                                                             QtMultimedia.LinearVolumeScale)
        }
        Button{
            Layout.column: 2
            Layout.row: 3
            
            id: openButton
            
            text: qsTr("...")
            Layout.preferredWidth: openButton.implicitHeight
            onClicked: fileDialog.open()
            
            FileDialog {
                id: fileDialog
                
                folder : videoUrl
                title: qsTr("Open file")
                nameFilters: [qsTr("MP4 files (*.mp4)"), qsTr("All files (*.*)")]
                onAccepted: mediaplayer.source = fileDialog.fileUrl
            }
            
            
        }
        
        Label{
            id:positionLabel
            readonly property int minutes: Math.floor(mediaplayer.position / 60000)
            readonly property int seconds: Math.round((mediaplayer.position % 60000) / 1000)
            text: Qt.formatTime(new Date(0, 0, 0, 0, minutes, seconds), qsTr("mm:ss"))
        }
        
        
        
        
        
    }
    //            Button{
    //                text: "But1"
    //                anchors.right: parent.right
    //                anchors.left: but2.right
    //                anchors.margins: 40
    //                anchors.top: but2.bottom
    //                height: 50
    //                //                просто интарактивно манипулируют координатами и размерами комапанентов
    
    //            }
    //            Button{
    //                id: but2
    //                text: "But2"
    //                anchors.left: parent.left
    //            }
    //            Button{
    //                id: but3
    //                text: "But3"
    //                anchors.centerIn: parent
    //                height: 93
    //                width: 100
    //            }
    //            Button{
    //                id: but4
    //                text: "But4"
    //                anchors.horizontalCenter:  parent.horizontalCenter
    //                anchors.verticalCenter: parent.verticalCenter
    //                height: 200
    //                width: 100
    //            }
    
}
