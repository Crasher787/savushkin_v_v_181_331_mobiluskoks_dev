#include "httpcontroller.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QtWidgets/QTableView>
#include <QCryptographicHash>
#include <mailmodel.h>


HttpController::HttpController(QObject *QMLObject) : showHttp(QMLObject)
{
    nam = new QNetworkAccessManager(this); // создаем менеджер
    //  connect(nam, &QNetworkAccessManager::finished, this, &HttpController::SlotFinished);
    connect(nam, &QNetworkAccessManager::finished, this, &HttpController::onNetworkValue);
   //  connect(nam, &QNetworkAccessManager::finished, this, &HttpController::noSuccess);
    mail_model = new MailModel();
}

void HttpController::getNetworkValue()
{ // запрос к сайту
    QNetworkRequest request;
    request.setUrl(QUrl("https://www.worldofwargraphs.com/ru/"));
    qDebug() << request.url();
    QNetworkReply * reply;
    QEventLoop evntLoop;
    connect(nam, &QNetworkAccessManager::finished, &evntLoop, &QEventLoop::quit);
    reply = nam->get(request);
    evntLoop.exec();
    QString replyString = reply->readAll();
    // emit signalSendToQML(QString(replyString));
}


void HttpController::onNetworkValue(QNetworkReply *reply)
{ // парсинг сайта
     QString str = reply->readAll(); // записывем в str  сайт
     QObject* textField = showHttp->findChild<QObject*>("textField"); // ищем элемент textField, куда буем записывать
     QObject* textFieldDate = showHttp->findChild<QObject*>("textFieldDate");
     QObject* textArea = showHttp->findChild<QObject*>("textArea"); // ищем элемент textArea, откуда будем брать
     textArea->setProperty("text", str);
      // задаем параметр "текст" для textArea из qml
     int j = 0;
      if((j = str.indexOf("icon-24-race_10_0", j)) != -1) // ищем индекс этого тега
      {
         textField->setProperty("text", str.mid( j + 96,10));
         textFieldDate->setProperty("text", str.mid( j + 280,6));
      }
}


bool HttpController::failed (QString add){

    qDebug() << "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
    if(add.indexOf("st._hi=") != -1)
    {
        QString pop;
        pop = add.split("st._hi=")[1].split(" ")[0];
          return 1;
    }
    else {
          return 0;
    }
     return 0;
}

bool HttpController::cancel (QString add){

    if(add.indexOf("error=") != -1)
    {
      QString pop;
        pop = add.split("error=")[1].split(" ")[0];

       return 1;
    }
    else {
        return 0;
    }
   return 0;
}


void HttpController::hashMD5(QString add){
    qDebug() <<"вывод пукалей"<< add;
    if (add.indexOf("session_secret_key=") != -1) // условие если он найден то записать в переменну
      {
           session_secr = add.split("session_secret_key=")[1].split("&")[0]; //запись в переменную токена
           qDebug() << "Наш сикрет: " << session_secr;
           QString param = "application_key=CDGGDNJGDIHBABABAformat=jsonmethod=photos.getPhotos"+session_secr;
              qDebug() << "Наш параметр" << param;
           QByteArray array;
           array.append(param);
           qDebug() << "Наш массив" << array;
           myHashMd5 = QString(QCryptographicHash::hash((array),QCryptographicHash::Md5).toHex());
           qDebug() << "Наш хеш" << myHashMd5;
      }
      else{
          qDebug() << "Ошибка";
//          m_accessToken = "вы не вошли";
//          return m_accessToken;

      }
}


QString HttpController::success (QString add){ // функия для вывода access_token

    qDebug() <<"вывод пукалей"<< add;
    if (add.indexOf("access_token=") != -1) // условие если он найден то записать в переменну
      {
           m_accessToken = add.split("access_token=")[1].split("&")[0]; //запись в переменную токена
           qDebug() << "Наш токен: " << m_accessToken;
           return m_accessToken;
      }
      else{
          qDebug() << "Ошибка";
//          m_accessToken = "вы не вошли";
//          return m_accessToken;

      }

//    if(add.indexOf("fail=") != -1)
//    {
//        qDebug() <<"метод работает но не работотает условие";
//        m_accessToken = add.split("fail=")[1].split(" ")[0];
//        qDebug() << "вывод ошибки" << m_accessToken;
//        return "Вы не вошли";

//    }
//    else {
//        qDebug() << "Ошибка не произошла";
//    }

    QString str = " ";
    return str;
}

void HttpController::restRequest(){

    QEventLoop loop;
    nam = new QNetworkAccessManager();

    QObject::connect(nam, // связываем loop  с нашим менеджером
                     SIGNAL(finished(QNetworkReply*)),
                     &loop,
                     SLOT(quit()));

//       qDebug() << "Наш токен: " << m_accessToken;
//       qDebug() << "Наш хеш" << myHashMd5;
    QNetworkReply *reply = nam->get(QNetworkRequest(QUrl( "https://api.ok.ru/fb.do?application_key=CDGGDNJGDIHBABABA&format=json&method=photos.getPhotos"
                                                          "&sig="+ myHashMd5 +
                                                          "&access_token="+ m_accessToken  )));

     // qDebug() << "Наша nam" << nam;
    loop.exec();
   // QString photo(reply->readAll());

   //  qDebug() << "Наша URL-ka" << reply;
   //  qDebug() << "*** Список друзей в формате json ***" << photo;

//    // вся строка JSON с сервера грузится в QJsonDocument
      QJsonDocument document = QJsonDocument::fromJson(reply->readAll());

     // qDebug() <<"Наш document"<< document;
      QJsonObject root = document.object();
      qDebug() <<"Наш root"<< root;
      QJsonValue itog = root.value("photos");
     qDebug() <<"Photos"<< itog;
      //  QJsonObject itog1 = itog.toObject();
       //qDebug() << itog1;
     // QJsonValue itog2 = itog1.value("items");
       //qDebug() << itog2;
      QJsonArray smth = itog.toArray();
   //  qDebug()<<"хз что тут будет" << smth;
     // Перебираем все элементы массива
       for(int i = 0; i < smth.count(); i++){

        QJsonObject znach = smth.at(i).toObject();
//       // Забираем значения свойств имени
         QString userid = znach.value("user_id").toString();
         qDebug() << userid;

//       // Забираем значения свойств фамилии
         QString textp = znach.value("text").toString();
        qDebug() << textp;

//       // Забираем значения id
         int commentscount = znach.value("comments_count").toInt();
         qDebug() << commentscount;

//       // Забираем ссылку на главное фото
         QUrl photo = znach.value("pic50x50").toString();
         qDebug() << photo;

      mail_model->addItem(MailObject (userid, textp, photo, commentscount ));
       qDebug()<<"yfi ger" << mail_model->Mailuserid;
        qDebug() << mail_model->Mailtextp;
       qDebug() << mail_model->MailPhoto;
       qDebug() << mail_model->Mailcommentscount;

   }
}


