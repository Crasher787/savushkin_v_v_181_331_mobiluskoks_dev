import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2 // подсказка конт пробел
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.12
import QtWebView 1.1

Page{
    id: page5
    header: ToolBar{
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 50
            implicitWidth: 100
            color: "#00a88e"
        }
        GridLayout{
            
            columns: 3
            anchors.fill:parent
            
            ToolButton {
                Layout.alignment: Qt.AlignLeft
                Layout.columnSpan: 3
                
                text: qsTr("⇦")
                x: 10
                y: 1
                font.pixelSize: 40
                
            }
            
            Label{
                Layout.columnSpan: 3
                Layout.row: 0
                text: "Работа с HTTPs OAuth"
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }
            
            Row
            {
                Layout.column: 2
                Layout.alignment: Qt.AlignRight
                ToolButton {
                    
                    text: qsTr("✮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                    onClicked:{
                        success(final_ac.text);
                        //webView.goBack();
                        restRequest();
                    }
                }
                
                ToolButton {
                    
                    text: qsTr("⋮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                    onClicked: {
                        
                    }
                    
                }
            }
            //
        }
    }
    
    ColumnLayout{

        anchors.fill: parent
        
        WebView{
            Layout.row: 3
            id: webView
           // url:"https://connect.mail.ru/oauth/authorize?client_id=772182&response_type=token&redirect_uri=http%3A%2F%2Fconnect.mail.ru%2Foauth%2Fsuccess.html"
             url:"https://connect.ok.ru/oauth/authorize?client_id=512000537954&scope=VALUABLE_ACCESS;LONG_ACCESS_TOKEN&response_type=token&redirect_uri=https://apiok.ru/oauth_callback"
            Layout.fillWidth: true
            Layout.fillHeight: true
            onLoadingChanged: {
                final_ac.text = webView.url
                console.info("*** "+ webView.url)

                success(webView.url)
                failed(webView.url)
                hashMD5(webView.url)
                cancel(webView.url)

                var token = httpController.success(webView.url)
                var puken = httpController.failed(webView.url)
                var cancell = httpController.cancel(webView.url)

                if(token !== " "){

                    tokens.text = token
                    tokens.visible = token === " " ? false : true;
                    mailButton.visible = token === " " ? false : true;
                    webView.visible = token === " " ? true : false;
                    successtoken.visible = token === " " ? false : true;
                }
                else if (puken === true){

                    webView.visible = puken === false ? true : false;
                    failedpuken.visible = puken === false ? false : true;
                    buttonreturn.visible = puken === false ? false : true;
                }
                else
                {
                    webView.visible = cancell === false ? true : false;
                    labelreturn.visible = cancell === false ? false : true;
                    buttonreturn.visible = cancell === false ? false : true;


                }

            }
        }
        Label{

            visible: false

            font.pixelSize: 9
            Layout.alignment: Qt.AlignCenter
            objectName: "TokenPokesh"
            id:tokens
        }
        Button{
            visible: false
            id:mailButton
            font.pixelSize: 13
            text: "🌈Показать Фоточки🌅"
            Layout.row:3
            Layout.alignment: Qt.AlignCenter
            onClicked:{
                //                     success(final_ac.text);
                //webView.goBack();
                       restRequest();
            }
        }
        Button{
            visible: false
            id:buttonreturn
            text: "Повтрить попытку"
            Layout.row:3
            Layout.alignment: Qt.AlignCenter
            onClicked:{
                webView.url = "https://connect.ok.ru/oauth/authorize?client_id=512000537954&scope=VALUABLE_ACCESS;LONG_ACCESS_TOKEN&response_type=token&redirect_uri=https://apiok.ru/oauth_callback"
              buttonreturn.visible = false
                webView.visible = true
                labelreturn.visible = false
                failedpuken.visible = false
            }
        }
        Label{
            visible: false
            Layout.row: 4
            Layout.alignment: Qt.AlignCenter
            id:labelreturn
             font.pixelSize: 30
            text: "💢Вы отменили попытку💢"
        }
        Label{
            visible: false
            Layout.row: 4
            Layout.alignment: Qt.AlignCenter
            id:failedpuken
            font.pixelSize: 30
            text: "❌Ошибка при логине❌"
        }

        Label{
            visible: false
            Layout.row: 4
            Layout.alignment: Qt.AlignCenter
            id:successtoken
            font.pixelSize: 40
            text: "🥒Вы вошли🥒"
            
        }
        
        Label{
            Layout.row: 1
            id: final_ac
            Layout.alignment: Qt.AlignCenter
            visible: true
            Layout.fillWidth: true
        }

    }
}
