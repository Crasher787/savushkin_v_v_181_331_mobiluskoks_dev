#ifndef QHTTPCONTROLLER_H
#define QHTTPCONTROLLER_H

#include <QObject>
#include <QNetworkAccessManager>
#include "mailmodel.h"
#include <QCryptographicHash>

class HttpController : public QObject

{
    Q_OBJECT
public:
    explicit HttpController(QObject *parent = nullptr);

    QNetworkAccessManager *nam;
    QString m_accessToken; // полученный access_token
    MailModel *mail_model; // наша модель
    QString session_secr; //получение секрета
    QString myHashMd5; // получение хеша


signals:
    //    void signalSendToQML(QString pReply);

public slots:
    void onNetworkValue(QNetworkReply *reply);
    void getNetworkValue();
    //  void SlotFinished(QNetworkReply *reply);
     QString success (QString add);
     bool failed (QString add);
     bool cancel (QString add);
     void restRequest();
     void hashMD5(QString add);
//    void enterSuccess(QString pop);
//    void noSuccess(QNetworkReply *reply);

protected:
    QObject *showHttp;

};

#endif // QHTTPCONTROLLER_H
