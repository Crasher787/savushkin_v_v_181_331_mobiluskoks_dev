import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2 // подсказка конт пробел
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.12
import QtWebView 1.1

Page {
    id:page1
    title: qsTr("Number 1")
    
    
    header: ToolBar{
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 50
            implicitWidth: 100
            color: "#00a88e"
        }
        GridLayout{
            columns: 3
            anchors.fill:parent
            ToolButton {
                Layout.alignment: Qt.AlignLeft
                Layout.columnSpan: 3
                id: points3
                text: qsTr("⇦")
                x: 10
                y: 1
                font.pixelSize: 40
            }
            
            Label{
                Layout.columnSpan: 3
                Layout.row: 0
                text: "Настройки"
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }
            
            Row
            {
                Layout.column: 2
                Layout.alignment: Qt.AlignRight
                ToolButton {
                    id: points2
                    text: qsTr("✮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                }
                
                ToolButton {
                    id: points1
                    text: qsTr("⋮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                    
                    
                    
                }
            }
            //
        }
        
        
        // два конкурируюших механизмов раскладки layout  и
        //            Механизм Anchors – упрощенный подход. Позволяет приязывать края одних компонентов к другим по цепочке (норм, если по высоте и ширине не более 3-х элементов)
        //            Layout основан на таближе 1 компанент нужно закинуть в баблицу 2 нужно задать строку столбец
        //            если компонент обявлять по очереде и не заданы строки сталбцы то компаненты автомотический будут размещатся
        //            с увилицением колонки и помере заполнения строк.
        
        
        
        
    }
    
    GridLayout {
        anchors.fill:parent
        id: grid
        //rowSpacing: 5
        columns: 3
        rows:4
        
        
        Row{
            
            Layout.alignment: Qt.AlignLeft
            height: 70
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            spacing: 0.8
            Layout.row: 0
            Layout.columnSpan: 3
            
            Text {
                
                width: 480
                text: qsTr("Здесь вы можете настроить уведомления,изменить настройки блокировки экрана, а также просмотреть информацию о приложении или удалить его.")
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 0
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignLeft
                textFormat: Text.AutoText
                font.pixelSize: 12
            }
            
        }
        
        
        RowLayout{
            
            Layout.row:1
            anchors.horizontalCenter: parent.horizontalCenter
            ColumnLayout{
                
                
                Layout.column:0
                
                
                
                Text {
                    id: element
                    Layout.alignment: Qt.AlignCenter
                    font.pointSize: 12
                    text: qsTr("Введите значение для поиска")
                    Layout.leftMargin: 10
                    Layout.topMargin: 0
                    leftPadding: 2
                }
                
                TextField {
                    Layout.leftMargin: 10
                    Layout.alignment: Qt.AlignCenter
                    
                    id: cont
                    placeholderText: qsTr("Enter description")
                    background: Rectangle {
                        implicitWidth: 150
                        implicitHeight: 40
                        color: cont.enabled ? "transparent" : "#353637"
                        border.color: cont.enabled ? "#21be2b" : "transparent"
                    }
                }
            }
            
            ColumnLayout{
                Layout.column:2
                Layout.row:1
                Label{
                    id: label
                    Layout.alignment: Qt.AlignCenter
                    text: "Громкость Уведомления"
                    Layout.bottomMargin: 0
                    Layout.topMargin: 4
                    font.pointSize: 12
                    horizontalAlignment: Text.AlignHCenter
                    
                }
                Slider {
                    id: slider
                    
                    from: 1
                    value: 25
                    to: 100
                }
                
            }
            
            
        }
        
        
        
        
        
        
        
        ColumnLayout{
            id: columnLayout1
            Layout.row: 2
            Layout.columnSpan: 3
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: 100
            Layout.preferredWidth: 100
            Text {
                y: 0
                height: 19
                Layout.alignment: Qt.AlignCenter
                font.pointSize: 12
                text: qsTr("Рингтон для оповещения об угрозе заражении вирусами.")
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            RowLayout{
                Layout.alignment: Qt.AlignCenter
                Tumbler {
                    id: control
                    width: 80
                    model: 6
                    background: Item {
                        Rectangle {
                            opacity: control.enabled ? 0.2 : 0.1
                            border.color: "#000000"
                            width: parent.width
                            height: 1
                            anchors.top: parent.top
                        }
                        
                        Rectangle {
                            opacity: control.enabled ? 0.2 : 0.1
                            border.color: "#000000"
                            width: parent.width
                            height: 1
                            anchors.bottom: parent.bottom
                        }
                    }
                    
                    delegate: Text {
                        text: qsTr("Рингтон %1").arg(modelData + 1)
                        font: control.font
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        opacity: 1.0 - Math.abs(Tumbler.displacement) / (control.visibleItemCount / 2)
                    }
                    
                    Rectangle {
                        anchors.horizontalCenter: control.horizontalCenter
                        y: control.height * 0.4
                        width: 60
                        height: 1
                        color: "#21be2b"
                    }
                    
                    Rectangle {
                        anchors.horizontalCenter: control.horizontalCenter
                        y: control.height * 0.6
                        width: 60
                        height: 1
                        color: "#21be2b"
                    }
                }
            }
        }
        
        
        ColumnLayout{
            id:coltut
            Layout.row:3
            Layout.columnSpan: 3
            Layout.alignment: Qt.AlignCenter
            Text {
                Layout.columnSpan: 3
                id: element1
                Layout.alignment: Qt.AlignCenter
                font.pointSize: 12
                text: qsTr("Индикатор проверки памяти")
                horizontalAlignment: Text.AlignHCenter
                transformOrigin: Item.Center
                
            }
            
            BusyIndicator {
                Layout.alignment: Qt.AlignCenter
                running: image.status === Image.Loading
            }
            
            
            
            
        }
        
        
        Button{
            // *#06#
            id: buton
            Layout.row: 4
            Layout.columnSpan: 3
            Layout.alignment: Qt.AlignCenter
            
            
            text: "Удаление информации"
        }
    }
}
