import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2 // подсказка конт пробел
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.12
import QtWebView 1.1




Page {
    id:page3
    header: ToolBar{
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 50
            implicitWidth: 100
            color: "#00a88e"
        }
        GridLayout{
            ColorDialog {
                                    id: colorDialog
                                    title: "Please choose a color"
                                    color: "white"

                                }


            columns: 3
            anchors.fill:parent
            ToolButton {
                Layout.alignment: Qt.AlignLeft
                Layout.columnSpan: 3
                id: points12
                text: qsTr("⇦")
                x: 10
                y: 1
                font.pixelSize: 40
            }

            Label{
                Layout.columnSpan: 3
                Layout.row: 0
                text: "работа с картинками"
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }

            Row
            {
                Layout.column: 2
                Layout.alignment: Qt.AlignRight
                ToolButton {
                    id: points13
                    text: qsTr("✮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                   onClicked: colorDialog.open()
                }

                ToolButton {
                    id: points14
                    text: qsTr("⋮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                    onClicked: fileDialog.open()



                }
            }
            //
        }
    }

    FileDialog {
                    id: fileDialog

                    title: qsTr("Open file")
                   // nameFilters: [qsTr("files (.png)"), qsTr("All files (.*)")]
                    onAccepted: {
                        bug.source = fileDialog.fileUrl
                        butterfly.source = fileDialog.fileUrl
                }
    }


    GridLayout{
         anchors.fill: parent
         columns: 2


        Rectangle{

            Layout.columnSpan: 2
            color: "#c9cbca"

           height: 30
           Layout.fillWidth: true
           radius: 5
           Text {
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.verticalCenter: parent.verticalCenter

               id: name
               text: qsTr("<b>Blur<b>")
           }


        }
        Item {

            Layout.fillHeight: true
            Layout.fillWidth: true

            Layout.alignment: Qt.AlignCenter


            Image {

                id: bug
                source: "camera/75d9494e555dc1b5e75e82b524da3559--herbal-vaporizer-best-vaporizer.jpg"
                sourceSize: Qt.size(parent.width, parent.height)
                Layout.alignment: Qt.AlignCenter
                smooth: true
                visible: false
            }

            LinearGradient {
                id: mask
                anchors.fill: bug
                gradient: Gradient {
                    GradientStop { position: 0.2; color: "#ffffffff" }
                    GradientStop { position: 0.5; color: "#00ffffff" }
                }
                start: Qt.point(0, 0)
                end: Qt.point(sliderend.value, 0)
                visible: false
            }

            MaskedBlur {
                anchors.fill: bug
                source: bug
                maskSource: mask

                radius: sliderblurradius.value
                samples: sliderblursamp.value
            }
        }

       ColumnLayout{


           Layout.fillWidth: true
           Layout.fillHeight: true
           Slider{
               Layout.alignment: Qt.AlignCenter
               Text {

                   text: qsTr("Radius")
               }
               id:sliderblurradius
               from: 0
               to: 50
               stepSize: 1


           }
           Slider{
               Layout.alignment: Qt.AlignCenter
               Text {

                   text: qsTr("Samples")
               }
               id:sliderblursamp
               from: 5
               to: 25
               stepSize: 1
           }
           Slider{
               Layout.alignment: Qt.AlignCenter
               Text {

                   text: qsTr("Width")
               }
               id:sliderend
               from:0
               to: 450
               stepSize: 1
           }

       }


        Rectangle{

            Layout.columnSpan: 2
            color: "#c9cbca"

           height: 30
           Layout.fillWidth: true
           radius: 5
           Text {
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.verticalCenter: parent.verticalCenter


               text: qsTr("<b>Shadow<b>")
           }



        }

        Item {
            Layout.alignment: Qt.AlignCenter
            Layout.fillHeight: true
            Layout.fillWidth: true


            Rectangle {
                anchors.fill: parent
            }

            Image {
                id: butterfly
                source:  "camera/75d9494e555dc1b5e75e82b524da3559--herbal-vaporizer-best-vaporizer.jpg"
                sourceSize: Qt.size(parent.width, parent.height)
                smooth: true
                visible: false
            }

            InnerShadow {
                anchors.fill: butterfly
                radius: sliderinerradius.value
                samples: 10
                horizontalOffset: sliderineroff.value
                verticalOffset: sliderineroffset.value
                color: colorDialog.color
                source: butterfly
            }
        }

        ColumnLayout{

            Slider{
                Text {

                    text: qsTr("Radius")
                }
                id:sliderinerradius
                from: 0
                to: 50
                stepSize: 1


            }

            Slider{
                Text {

                    text: qsTr("Horizontal Offset")
                }
                id:sliderineroff
                from: -10
                to: 16
                stepSize: 0.1
            }
            Slider{
                Text {

                    text: qsTr("Vertical Offset")
                }
                id:sliderineroffset
                from: -10
                to: 20
                stepSize: 0.1
            }



        }



        Rectangle{

            color: "#c9cbca"
            Layout.columnSpan: 2
           height: 30
           Layout.fillWidth: true
           radius: 5
           Text {
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.verticalCenter: parent.verticalCenter


               text: qsTr("<b>Rectangle<b>")
           }


        }
        Item {
            Layout.alignment: Qt.AlignCenter

            Layout.fillWidth: true
            Layout.fillHeight: true


            Rectangle {
                id: background
                anchors.fill: parent
                color: "black"
            }

            RectangularGlow {
                id: effect
                anchors.fill: rect
                glowRadius: sliderractangleRad.value
                spread: sliderRactangspread.value
                 color: colorDialog.color
                cornerRadius: rect.radius + glowRadius
            }

            Rectangle {
                id: rect
                color: "black"
                anchors.centerIn: parent
                width: Math.round(parent.width / 1.5)
                height: Math.round(parent.height / 2)
                radius: 25

            }
        }

        ColumnLayout{



            Slider{
                Text {

                    text: qsTr("GlowRadius")
                }
                id:sliderractangleRad
                from: 0
                to: 10
                stepSize: 0.1


            }
            Slider{
                Text {

                    text: qsTr("Spread")
                }
                id:sliderRactangspread
                from: 0
                to: 10
                stepSize: 0.1
            }


        }

    }
}
