#ifndef ENCRIPTIONCONTROLLER_H
#define ENCRIPTIONCONTROLLER_H
#include <QString>

class EncriptionController
{
public:
    EncriptionController();
   public:
    bool EncryptionFile(const QString & in_file, const QString & out_file);
    bool DecryptionFile(const QString & in_file, const QString & out_file);
};

#endif // ENCRIPTIONCONTROLLER_H
