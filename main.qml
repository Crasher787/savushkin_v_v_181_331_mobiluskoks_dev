import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2 // подсказка конт пробел
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.12
import QtWebView 1.1

ApplicationWindow {
    id:mainWindow //индификатор для обращения
    visible: true
    signal signalMakeRequest();
    signal success (string add);
    signal failed(string pop);
    signal cancel (string pop);
    signal restRequest();
    signal hashMD5(string pop);

    width: 480
    height: 690
//    property alias columnLayout1: columnLayout1
//    property alias slider: slider
    title: qsTr("MyFirstProgMob")//qsTr подставляет нужную страку языка

//    Connections {
//            target: httpController

//            function onSignalSendToQML(pString) {
//                textarea.append(pString);
//            }
//        }

    SwipeView {
        id: swipeView
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        currentIndex: tabBar.currentIndex


        Page1 {
            id: page1
        }

        Page2 {
            id: page2
        }

        Page3 {
            id: page3
        }

        Page4 {
            id: page4
        }

        Page5 {
            id: page5
        }

        Page6 {
            id: page6
        }
        Page7 {
            id: page7
        }
        Page8 {
            id: page8
        }
        Page9 {
            id: page9
        }
    }


    Drawer{
        id:drawer

        width: 0.66 * parent.width
        height: parent.height
        GridLayout{

            rows:15

            Button{
                Layout.row: 1
                text: qsTr("ЛР 1")
               onClicked: {

                   swipeView.currentIndex =0
                   drawer.close()
               }


            }
            Button {
                Layout.row: 2
                text: qsTr("Page 2")
                onClicked: {

                    swipeView.currentIndex =1
                    drawer.close()
                }
            }
            Button {
                Layout.row: 3
                text: qsTr("Page 3")
                onClicked: {

                    swipeView.currentIndex =2
                    drawer.close()
                }
            }
            Button {
                Layout.row: 4
                text: qsTr("Page 4")
                onClicked: {

                    swipeView.currentIndex =3
                    drawer.close()
                }
            }
            Button {
                Layout.row: 4
                text: qsTr("Page 5")
                onClicked: {

                    swipeView.currentIndex =4
                    drawer.close()
                }
            }

            Button {
                Layout.row: 4
                text: qsTr("Page 6")
                onClicked: {

                    swipeView.currentIndex =5
                    drawer.close()
                }
            }



        }

    }




//    footer: TabBar {
//        id: tabBar
//        currentIndex: swipeView.currentIndex

//        TabButton {
//            text: qsTr("ЛР 1")
//        }
//        TabButton {
//            text: qsTr("Page 2")
//        }
//        TabButton {
//            text: qsTr("Page 3")
//        }

//    }
}

/*##^##
Designer {
    D{i:32;anchors_height:19;anchors_width:467;anchors_x:"-137";anchors_y:0}
}
##^##*/
