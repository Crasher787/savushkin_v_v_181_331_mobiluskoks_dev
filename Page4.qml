import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2 // подсказка конт пробел
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.12
import QtWebView 1.1

Page{
    id:page4
    
    header: ToolBar{
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 50
            implicitWidth: 100
            color: "#00a88e"
        }
        GridLayout{
            
            columns: 3
            anchors.fill:parent
            ToolButton {
                Layout.alignment: Qt.AlignLeft
                Layout.columnSpan: 3
                
                text: qsTr("⇦")
                x: 10
                y: 1
                font.pixelSize: 40
            }
            
            Label{
                Layout.columnSpan: 3
                Layout.row: 0
                text: "Работа с HTTPs"
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }
            
            Row
            {
                Layout.column: 2
                Layout.alignment: Qt.AlignRight
                ToolButton {
                    
                    text: qsTr("✮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                    onClicked: colorDialog.open()
                }
                
                ToolButton {
                    
                    text: qsTr("⋮")
                    x: 10
                    y: 1
                    font.pixelSize: 40
                    
                    
                    
                }
            }
            //
        }
    }
    
    
    GridLayout {
        anchors.fill: parent
        columns: 2
        
        Button {
            
            
            id: sent
            Layout.alignment: Qt.AlignCenter
            Layout.columnSpan: 2
            
            onClicked: {
                _send.getNetworkValue();
            }
            
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("отправить")
            }
        }
        
        ScrollView {
            Layout.fillHeight: true
            Layout.columnSpan: 2
            Layout.fillWidth: true
            clip:  true
            TextArea {
                id: textArea
                textFormat: Text.PlainText
                objectName: "textArea"
                readOnly: true
                anchors.fill: parent
                background: Rectangle {
                    color: "#eaeaea"
                }
            }
        }
        
        Label {
            Layout.alignment: Qt.AlignCenter
            //Layout.fillWidth: true
            Layout.columnSpan: 2
            text: "<b>Лучшие PVP расы<b>"
        }
        RowLayout{
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignCenter
            
            TextField {
                id: textField
                objectName: "textField"
                horizontalAlignment: Text.AlignHCenter
                readOnly: true
                Layout.alignment: Qt.AlignCenter
                Layout.columnSpan: 2
            }
            
            
            TextField {
                id: textFieldDate
                objectName: "textFieldDate"
                readOnly: true
                Layout.alignment: Qt.AlignCenter
                Layout.columnSpan: 2
                
            }
            
            
        }
    }
    
    
    
    
}
